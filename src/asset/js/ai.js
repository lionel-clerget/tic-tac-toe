// ---- INIT ----
function initAi() {
    currAiWinSeq = [];
    nextAiWinPos = [];
    aiHasWinPos = false;
    movePriority = false;
    player2.changeName("AI");
    for (var i = 0; i < winPos.length; i++) {
        nextAiWinPos.push(i);
    }
    getRandAiWinSeq();
}

// ---- UTILITIES ----
function getRandInt() {
    return Math.floor(Math.random() * nextAiWinPos.length)
}

function getGridObj(pos) {
    for (let i = 0; i < gridData.length; i++) {
        if (gridData[i].pos == pos) {
            return gridData[i]
        }
    }
}

function storeAiWinSeq(winSeq, idx) {
    currAiWinSeq = Array.from(winSeq);
    currAiIdx = idx;
    nextAiWinPos.splice(idx,1);
    aiHasWinPos = true;
    console.log(currAiWinSeq);
}

// Prend une valeur random dans nextAiWinPos (initialisé à chaque début de partie avec les index de winPos). Parcours le tableau winPos et attribut sa valeur à currAiWinSeq. Enleve l'index de winPos à nextAiWinPos pour qu'il ne puisse plus être "randomizé"
function getRandAiWinSeq() {
    console.log('getRandAiWinSeq');
    if (nextAiWinPos.length > 0) {
        let randIdx = nextAiWinPos[getRandInt()];
        for (let idx = 0; idx < winPos.length; idx++) {
            if (idx == randIdx) {
                storeAiWinSeq(winPos[idx], idx);
                return
            }
        }
    }else {
        getFreePos();
    }
}

// Cherche un Sequence gagnante
function foundAiWinSeq() {
    if (nextAiWinPos.length > 0) {
        for (let idx = 0; idx < nextAiWinPos.length; idx++) {
            let apply = true;
            for (let pos = 0; pos < winPos[idx].length; pos++) {
                for (var a = 0; a < aiPlayerPos.length; a++) {
                    if (winPos[idx][pos] == aiPlayerPos[a]) {
                        nextAiWinPos.splice(idx,1);
                        apply = false;
                        break
                    }
                }
            }
            if (apply) {
                storeAiWinSeq(winPos[idx]);
                return
            }
        }
        getFreePos();
    }else {
        getFreePos();
    }
}

// Pour le aiLevel 3 si joueur a 2 positions gagnantes trouve la position pour le contrer
function getAiCounterPos(arrPos) {
    for (let idx = 0; idx < winPos.length; idx++) {
        let count = 0;
        for (let pos = 0; pos < winPos[idx].length; pos++) {
            if (winPos[idx][pos] == arrPos[0] || winPos[idx][pos] == arrPos[1]) {
                count++
            }
            if (count == 2) {
                for (var p = 0; p < winPos[idx].length; p++) {
                    if (winPos[idx][p] != arrPos[0]) {
                        if (winPos[idx][p] != arrPos[1]) {
                            let obj = getGridObj(winPos[idx][p]);
                            if (obj.playerName == null) {
                                currAiWinSeq = []
                                currAiWinSeq[0] = obj;
                                return
                            }else {
                                movePriority = false;
                            }
                        }
                    }
                }
            }
        }
    }
}

function getFreePos() {
    console.log("getFreePos");
    for (var g = 0; g < gridData.length; g++) {
        if (gridData[g].playerName == null) {
            currAiWinSeq.push(gridData[g].pos);
            return
        }
    }
}

function isPlayerOnAiWinPos() {
    if (currPlayerPos != null) {
        if (currAiWinSeq.length != 0) {
            for (var c = 0; c < currAiWinSeq.length; c++) {
                if (currPlayerPos == currAiWinSeq[c]) {
                    currAiWinSeq.splice(currAiWinSeq[c], 1);
                    return true
                }
            }
            return false
        }else {
            return false
        }
    }else {
        return false
    }
}

// Si le tableau n'est pas vide --> enleve la dernière position au tableau currAiWinSeq
// Sinon agit en fonction du level de l'AI
function nextAiPos(oldPos) {
    if (currAiWinSeq.length != 0) {
        currAiWinSeq.splice(oldPos,1);
    }else {
        if (nbTour != TOTAL_TOUR) {
            if (nextAiWinPos.length != 0) {
                nextAiWinPos.splice(currAiIdx,1);
                if (aiLevel == 1 || aiLevel == 2) {
                    getRandAiWinSeq();
                }else {
                    foundAiWinSeq();
                }
            }else {
                getFreePos();
            }
        }
    }
}

// Lancée à chaque tour de l'AI
function aiMove() {
    console.log("aiMove");

    if (aiLevel == 2) {
        if (isPlayerOnAiWinPos()) {
            nextAiWinPos.splice(currAiIdx,1);
            getRandAiWinSeq();
        }
    }
    if (aiLevel == 3) {
        if (movePriority == true) {
            console.log(currAiWinSeq[0]);
            checkSolution(currAiWinSeq[0]);
            nextAiPos(currAiWinSeq[0]);
            movePriority = false;
            aiPlay = false;
            return
        }else {
            if (isPlayerOnAiWinPos()) {
                nextAiWinPos.splice(currAiIdx,1);
                foundAiWinSeq();
            }
        }
    }
    if (currAiWinSeq.length == 0) {
        nextAiPos();
    }
    for (var c = 0; c < currAiWinSeq.length; c++) {
        let pos = currAiWinSeq[c];
        let gridObj = getGridObj(pos);
        if (gridObj.playerName == null) {
            checkSolution(gridObj);
            nextAiPos(pos);
            aiPlay = false;
            return
        }else {
            nextAiPos(pos);
            aiMove();
            return
        }
    }
}
