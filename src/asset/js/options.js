// ---- SCREEN MENU Option du jeux ----

// ---- LISTENER ----
document.addEventListener("click", function(event){
    if (screenVisible == true) {
        let parent = event.target.parentElement;

    // Boite Modale
        if (event.target.id == currMenu ||
            event.target.classList.contains('modal-btn-close') ||
            parent.classList.contains('modal-btn-close')) {
            if (endGame) {
                if (lastAi != isAi) {
                    resetGame(true);
                }else {
                    resetGame(false);
                }
            }
            UIkit.modal(getById(currMenu)).hide();
            screenVisible = false;
        }
        if (event.target.classList.contains("playBtn")) {
            playGame();
        }

    // MODAL MODE DE JEUX
        if (event.target.id == "jvj" && isAi) {
            toggleGameMode('jvj');
        }
        if (event.target.id == "ai" && !isAi) {
            toggleGameMode('ai');
        }

        if (isAiLevelOpen == true) {
            if (event.target.hasAttribute('data-level')) {
                let level = event.target.attributes['data-level'].value;
                aiLevel = parseInt(level);
                toggleAiBtn(event.target, false);
            }
        }else {
            if (event.target.hasAttribute('data-level')) {
                uiSound.playSound('error',0);
            }
        }

    // MODAL CHOIX DU PION
        if (event.target.tagName == "IMG") {

            chooseToken(event.target);
            uiSound.playSound('click',0);
        }

        if (event.target.name === "token" || event.target.name === "bg") {
            chooseToken(event.target.firstChild);
            uiSound.playSound('click',0);
        }

        // Choix du son
        if (event.target.hasAttribute('data-sound')) {
            let player = event.target.attributes['name'].value;
            let sound = event.target.attributes['data-sound'].value;
            if (player == "player1") {
                player1.sound = parseInt(sound);
            }else {
                player2.sound = parseInt(sound);
            }
            uiSound.playSound(null, parseInt(sound))
        }
        // Son sur Element
        if (event.target.classList.contains('acc')) {
            uiSound.playSound('open_acc',0);
        }
        if (event.target.classList.contains('switch') ||
            parent.classList.contains('switch') ||
            event.target.tagName == "svg") {
            uiSound.playSound('click',0);
        }
    }

    // MENU INGAME
    if (event.target.hasAttribute("data-inGame")) {
        console.log('data-inGame');
        currMenu = event.target.attributes['uk-toggle'].value.split('#')[1];

        // Notification si changement mode de jeux
        if (!isAdvertShow) {
            let data = event.target.getAttribute("data-inGame");
            if (data == "false" && event.target.id == 'advert') {
                UIkit.notification({
                    message: 'Le changement de mode de jeux \nreinitialise votre score\n',
                    status: 'danger',
                    pos: 'top-center',
                    timeout: 7000
                });
            }
            // isAdvertShow = true;
        }

        // Gestion apparition modal & mode de jeux
        if (!screenVisible) {
            screenVisible = true;
            lastAi = isAi;
            lastMenu = currMenu;
        }else {
            if (lastMenu == currMenu) {
                screenVisible = false;
                if (endGame) {
                    if (lastAi != isAi) {
                        resetGame(true);
                    }else {
                        resetGame(false);
                    }
                }
            }else {
                lastMenu = currMenu;
            }
        }
        uiSound.playSound('open',0);
    }

    // Boite Modale Gagne ou égalité
    if (event.target.id == "modal-win"){
        resetGame(false);
    }

    // console.log(event);
});

// Au changement noms joueurs
document.addEventListener("input", function(event){
    if (event.target.id == "pName1") {
        player1.changeName(event.target.value);
    }
    if (event.target.id == "pName2") {
        player2.changeName(event.target.value);
    }
});
// ---- FIN LISTENER ----

function toggleGameMode(strMode) {
    isAi = !isAi;
    isAiLevelOpen = isAi;
    let otherMode = (strMode === 'ai') ? "jvj" : "ai";

    getById(strMode).classList.add('select');
    getById(otherMode).classList.remove('select');
    getById('aiLevel').classList.toggle('select');
    uiSound.playSound('open',0);
}

function toggleAiBtn(target, isReset) {
    let btnLevel = document.querySelectorAll('[data-level]');
    for (var i = 0; i < btnLevel.length; i++) {
        if (btnLevel[i].classList.contains('select')) {
            btnLevel[i].classList.toggle('select');
        }
    }
    if (!isReset) {
        uiSound.playSound('click',0)
        target.classList.toggle('select');
    }
}

// Au click choix du jeton
function chooseToken(eTarget) {
    let parent = eTarget.parentElement
    let playerAttrValue = parent.attributes["data-img"].value;

    if (!parent.classList.contains('select')) {
        if (playerAttrValue == "bg") {
            toggleElem(playerAttrValue, eTarget, parent.classList, true);
        }else {
            toggleElem(playerAttrValue, eTarget, parent.classList, false);
        }
    }
}

// Split le chemin de l'image parr le caractère '/' et garde le dernier élément --> nomImage.png
function getLast(imgSrc) {
    let split = imgSrc.split('/').reverse()[0];
    return split
}

function toggleElem(playerAttrValue, target, parentClass, isBg) {
    if (!parentClass.contains("muted") && !parentClass.contains("select")) {
        // data store
        if (playerAttrValue == "player1") {
            player1.changeTokenImg(target.src);
        }else if (playerAttrValue == "player2") {
            player2.changeTokenImg(target.src);
        }else {
            changeBg(target.src);
        }
        toggleSelect(playerAttrValue, target);
        if (!isBg) {
            disableToken(playerAttrValue, target.src);
        }
    }
}

function toggleSelect(playerAttrValue, target) {
    let buttons = document.querySelectorAll(`button[data-img="${playerAttrValue}"]`);
    buttons.forEach(function(elem) {
        if (elem.classList.contains('select')) {
            elem.classList.toggle('select');
        }
    });
    target.parentElement.classList.toggle('select')
}

function disableToken(playerAttrValue, img){
    // selectionne toutes les balises HTML qui ont l'attribut data-img mais dont la valeur n'est pas = à player et disactive le bouton (l'autre joueur ne peut plus le séléctionner)
    let buttonsMuted = document.querySelectorAll(`button[data-img]:not([data-img="${playerAttrValue}"])`);

    buttonsMuted.forEach(function(elem) {
        // choisit la première balise enfant --> image et compare le chemin de l'image
        let child = elem.firstElementChild;
        if (child.src == img) {
            elem.classList.add("muted");
        }else {
            elem.classList.remove("muted");
        }
    });
}

function initBg() {
    bg.body = document.getElementsByTagName('BODY')[0];
    let grid = document.getElementById('gridBg');
    for (var i = 0; i < bg.src.length; i++) {
        let classToInsert = "";
        if (i == 0) {
            classToInsert = "select ";
        }
        grid.insertAdjacentHTML('beforeend', '<div class="uk-width-1-3"><button class="btn '+ classToInsert +'uk-button" type="button" name="bg" data-img="bg"><img src="'+ bg.src[i] +'" alt=""></button></div>');
    }
}

function changeBg(src) {
    bg.body.style.backgroundImage = "url("+src+")";
}

// Mute les option du Menu inGame si la partie est en cours
function optionMuted() {
    for (var i = 0; i < btnOption.length; i++) {
        let canOpen = btnOption[i].getAttribute("data-inGame");
        if (canOpen == "false") {
            let parent = btnOption[i].parentElement;
            btnOption[i].classList.toggle('muted');

            if (btnOption[i].classList.contains('muted')) {
                parent.setAttribute("uk-tooltip", "title: Désactivé en cours de partie; pos: right");
            }else {
                parent.removeAttribute("uk-tooltip");
            }
        }
    }
}
