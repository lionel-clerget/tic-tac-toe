class Player {
    constructor(number) {
        this.name = "";
        this.number = number;
        this.tokenSrc =  "";
        this.sound = 0;
        this.titles = null;
        this.imgObj = null;
        this.card = null;
        this.scrore = 0;
        this.scoreObj = null;
        this._init();
    }

    _init(){
        this.titles = document.getElementsByClassName("player-name"+ this.number +"");
        this.imgObj = document.getElementsByClassName("img-p"+ this.number +"");
        this.card = getById("card"+ this.number +"");
        this.scoreObj = getById("pScore"+ this.number +"");
        this.changeScore(true);
    }
    changeTokenImg(newTokenSrc){
        this.tokenSrc = newTokenSrc;
        for (var i = 0; i < this.imgObj.length; i++) {
            this.imgObj[i].src = newTokenSrc;
        }
    }
    changeName(strName) {
        this.name = strName;
        for (var i = 0; i < this.titles.length; i++) {
            this.titles[i].innerHTML = strName;
        }
    }
    changeScore(isReset){
        this.score ++;
        if (isReset) {
            this.score = 0;
        }
        this.scoreObj.innerHTML = this.score.toString();
    }
}

class Token {
    constructor(elem, pos) {
        this.elem = elem;
        this.src;
        this.pos = pos;
        this.playerName = null;
    }
    changeImgSrc(){
        this.elem.src = this.src
    }
    resetToDefault(){
        this.elem.src = data["imgSrc"][2];
        this.playerName = null;
    }
}

class UISound {
    constructor() {
        this.elem = new Audio();
        this.src = ["asset/sound/play.mp3", "asset/sound/click_menu.mp3", "asset/sound/incorrect.mp3", "asset/sound/bubble.mp3", "asset/sound/win.mp3", "asset/sound/equality.mp3", "asset/sound/loose.mp3", "asset/sound/ui_open.mp3", "asset/sound/accordion.mp3"];
        this._init();
    }
    _init(){
        for (var i = 1; i < 7; i++) {
            this.src.push("asset/sound/pop-"+i+".mp3")
        }
    }
    playSound(str, num){
        this.elem.pause();
        if (num == 0) {
            switch (str) {
                case 'click':
                    this.elem.src = this.src[1]
                    break;
                case 'error':
                    this.elem.src = this.src[2]
                    break;
                case 'bubble':
                    this.elem.src = this.src[3]
                    break;
                case 'win':
                    this.elem.src = this.src[4]
                    break;
                case 'equality':
                    this.elem.src = this.src[5]
                    break;
                case 'loose':
                    this.elem.src = this.src[6]
                    break;
                case 'open':
                    this.elem.src = this.src[7]
                    break;
                case 'open_acc':
                    this.elem.src = this.src[8]
                    break;
                default:
                    this.elem.src = this.src[0]
            }
        }
        else {
            this.elem.src = this.src[8+num]
        }
        this.elem.load()
        this.elem.play();
    }
}


// Fonctions générales
function getById(id) {
    return document.getElementById(id);
}

document.addEventListener("DOMContentLoaded", function() {
    init();
});

// ---- INIT ----
function init() {
    imgObj = document.querySelectorAll('[data-jeton]');
    btnOption = document.querySelectorAll('[data-inGame]');
    uiSound = new UISound();
    initPlayers();
    initGridData();
    createElemSound()
    createElemToken();
    initBg();
    UIkit.modal(getById('modal-title')).show();
}

function initPlayers() {
    player1 = new Player(1);
    player2 = new Player(2);
    player1.tokenSrc = data["imgSrc"][0];
    player2.tokenSrc = data["imgSrc"][1];
    player1.name = "Joueur 1";
    player2.name = "Joueur 2";
    player1.sound = 1;
    player2.sound = 2;
}

// Sotck les objet Token dans gridData
function initGridData() {
    for (var i = 0; i < imgObj.length; i++) {
        let pos = imgObj[i].getAttribute('data-pos');
        gridData.push(new Token(imgObj[i], pos));
    }
}

function createElemSound() {
    let field;
    //Boucle pour les balises HTML soundPlayer1 & soundPlayer2
    for (var f = 0; f < 2; f++) {
        switch (f) {
            case 0:
                field = getById('soundPlayer1');
                p = 1;
                break;
            default:
                field = getById('soundPlayer2');
                p = 2;
        }
        // Boucle pour les sons
        for (var r = 1; r < 7; r++) {
            if (f == 0 && r == 1) {
                field.insertAdjacentHTML('beforeend', '<label><input class="uk-radio" type="radio" name="player'+p+'" data-sound="'+r+'" checked> Son '+r+'</label>');
            }else if (f == 1 && r == 2){
                field.insertAdjacentHTML('beforeend', '<label><input class="uk-radio" type="radio" name="player'+p+'" data-sound="'+r+'" checked> Son '+r+'</label>');
            }else {
                field.insertAdjacentHTML('beforeend', '<label><input class="uk-radio" type="radio" name="player'+p+'" data-sound="'+r+'"> Son '+r+'</label>');
            }
        }
    }
}

// Insert dans le HTML les différentes images des jetons
function createElemToken() {
    let nbToken = 3;
    let shapes = ["circle-", "cross-"];
    let grid;
    let p;
    //Boucle pour les balises HTML gridPlayer1 & gridPlayer2
    for (var g = 0; g < 2; g++) {
        switch (g) {
            case 0:
                grid = getById('gridPlayer1');
                p = 1;
                break;
            default:
                grid = getById('gridPlayer2');
                p = 2;
        }

        // Boucle pour les noms d'image
        for (var s = 0; s < shapes.length; s++) {
            // Boucle pour le nombre d'image
            for (var t = 1; t <= nbToken; t++) {
                // Insert les classes css sur les boutons pour séléctionner un jeton par défaut aux joueurs
                let classToInsert = "";
                if (s == 0 && t == 1) {
                    if (g == 0) {
                        classToInsert = "select ";
                    }else {
                        classToInsert = "muted ";
                    }
                }
                if (s == 1 && t == 1) {
                    if (g == 1) {
                        classToInsert = "select ";
                    }else {
                        classToInsert = "muted ";
                    }
                }
                grid.insertAdjacentHTML('beforeend', '<div class="uk-width-1-3"><button class="btn '+ classToInsert +'uk-button" type="button" name="token" data-img="player'+ p +'"><img src="asset/img/'+ shapes[s] + t +'.png" alt=""></button></div>');
            }
        }
    }
}
// ---- FIN DE L'INIT ----

// ---- LISTENER ----
document.addEventListener("click", function(event){

    // Au click sur les images de la grille
    if (!aiPlay && endGame == false) {
        if (event.target.hasAttribute("data-jeton")) {
            let pos = event.target.getAttribute("data-pos");
            for (let elem in gridData) {
                // lance la fonction si l'image est vide
                if (gridData[elem].pos == pos && gridData[elem].playerName == null) {
                    if (isAi) {
                        if (currPlayer == player1.name) {
                            player1.newPos = gridData[elem].pos;
                        }
                    }
                    checkSolution(gridData[elem]);
                }
            }
        }
    }
    // console.log(event);
});

// ---- DEBUT DU JEUX ----
function playGame() {
    console.log("playGame");
    getById("options").style.display = "block"
    toggleDisplayOnMenu();
    uiSound.playSound('play',0);
    screenVisible = false;
    endGame = false;

    player1.card.style.opacity = "1";
    player2.card.style.opacity = "0.25";
    checkPlayerName();
    resetGame(false);
}

// Enleve la navigation sur les bboite modal
function toggleDisplayOnMenu() {
    let modalFooter = document.getElementsByClassName('uk-modal-footer');
    for (var i = 0; i < modalFooter.length; i++) {
        modalFooter[i].style.display = 'none';
    }
    let btnClose = document.getElementsByClassName('modal-btn-close');
    for (var i = 0; i < btnClose.length; i++) {
        btnClose[i].style.display = "block";
    }
}

// Evite les bugs de conditions si la value et vide ou si les joueurs ont les mêmes noms
function checkPlayerName() {
    let players = [player1, player2];
    for (var i = 0; i < players.length; i++) {
        if (players[i].name == "") {
            players[i].name = data["player"][i];
        }
    }
    if (players[0].name == players[1].name) {
        players[0].name = data["player"][0]
        players[1].name = data["player"][1]
    }
    currPlayer = player1.name;
}

// Est lancée à chaque fin de tour
function checkSolution(gridObj) {
    if (isAi && currPlayer == player1.name) {
        currPlayerPos = gridObj.pos;
        if (aiLevel == 3) {
            aiPlayerPos.push(gridObj.pos);
        }
    }

    nbTour++;
    switchPlayer(gridObj);

    for (let idx = 0; idx < winPos.length; idx++) {
        for (let pos = 0; pos < winPos[idx].length; pos++) {
            // Si la position de l'image clické est dans le tableau winPos
            if (gridObj.pos == winPos[idx][pos]) {

                // Retourne vrai si un joueur a fait une combinaison gagnante du tableau winPos
                if (isAWinSequence(winPos[idx], gridObj.playerName)) {

                    // Alors le joueur a gagné et c'est la fin de la partie
                    onPlayerWin(gridObj, false);
                    endGame = true;
                    return
                }
            }
        }
    }
    // Check l'égalité
    if (nbTour != TOTAL_TOUR) {
        if (isAi && currPlayer == player2.name) {
            setTimeout(aiMove, 1000);
        }
    }else {
        onPlayerWin(gridObj, true);
        endGame = true;
    }
}

// Vérifie si la combinaison est gagnante
function isAWinSequence(winSequence, playerName) {
    let p = 0;
    let playerPos = [];
    // Boucle le tableau passé en paramètre et ajoute 1 à p si la position de l'objet est bonne et si l'objet contient le nom du joueur testé
    for (var i = 0; i < winSequence.length; i++) {
        let pos = winSequence[i];
        let objToken = getGridObj(pos);
        if (objToken.playerName == playerName) {
            p++
            if (isAi && aiLevel == 3) {
                if (playerName == player1.name) {
                    playerPos.push(pos);

                    if (p == 2 && movePriority == false) {
                        movePriority = true;
                        console.log("movePriority");
                        getAiCounterPos(playerPos);
                    }
                }
            }
        }
    }
    if (p == 3) {
        return true
    }else {
        return false
    }
}

// Attribut de nouvelles valeurs à l'objet et switch de joueur
function switchPlayer(obj) {
    if (nbTour != TOTAL_TOUR) {
        if (currPlayer == player1.name) {
            uiSound.playSound(null, player1.sound);
        }else {
            uiSound.playSound(null, player2.sound);
        }
    }
    switch (currPlayer) {
        case player2.name:
            obj.playerName = currPlayer;
            obj.src = player2.tokenSrc;
            player1.card.style.opacity = "1";
            player2.card.style.opacity = "0.25";
            currPlayer = player1.name;
            break
        default:
            obj.playerName = currPlayer;
            obj.src = player1.tokenSrc;
            player1.card.style.opacity = "0.25";
            player2.card.style.opacity = "1";
            currPlayer = player2.name;
    }
    obj.changeImgSrc();
}

// Fin de la partie, check si égalité ou le joueur gagnant
function onPlayerWin(objToken, isEqual) {
    let winBlock = getById('winPlayer');
    let result = getById('result');
    if (isEqual) {
        winBlock.innerHTML = player1.name+" & "+player2.name;
        result.innerHTML = "Égalité!";
        uiSound.playSound('equality',0);
    }else {
        let player;
        if (objToken.playerName == player1.name) {
            player = player1
        }else {
            player = player2
        }
        if (isAi && player == player2) {
            uiSound.playSound('loose',0);
        }else {
            uiSound.playSound('win',0);
        }
        player.changeScore(false);
        winBlock.innerHTML = player.name;
        result.innerHTML = "Gagne!!"
    }
    UIkit.modal(getById('modal-win')).show();
    optionMuted();
}

// Reinitialise les valeur par défaut et relance la partie
function resetGame(isResetScore) {
    if (endGame == true) {
        optionMuted();
    }
    endGame = false;
    nbTour = 0;
    for (var i = 0; i < gridData.length; i++) {
        gridData[i].resetToDefault();
    }
    if (isAi) {
        initAi();
        if (currPlayer == player2.name) {
            aiPlay = true;
            setTimeout(aiMove, 1000);
        }
    }else {
        if (player2.name == "AI") {
            player2.name = data["player"][1]
        }
    }
    if (isResetScore) {
        player1.changeScore(true);
        player2.changeScore(true);
    }
}
// ---- FIN FONCTION INGAME ----
