// Valeur par défaut
var data = {
    "player":["Joueur 1", "Joueur 2"],
    "imgSrc":["asset/img/circle-1.png", "asset/img/cross-1.png", "asset/img/empty.png"]
}
// Tableau des combinaisons gagnantes
var winPos = [
    ["tl","tm","tr"],
    ["ml","mm","mr"],
    ["bl","bm","br"],
    ["tl","ml","bl"],
    ["tm","mm","bm"],
    ["tr","mr","br"],
    ["tl","mm","br"],
    ["bl","mm","tr"]
]
var player1, player2, imgObj, btnOption;
var gridData = [];
var currPlayer;
var nbTour = 0;
const TOTAL_TOUR = 9;
var endGame = false;

// AI
var currAiWinSeq = [];
var currAiIdx = null;
var nextAiWinPos = [];
var aiPlayerPos = [];
var currPlayerPos = null;
var movePriority = false;
var aiLevel = 1;
var isAi = false;
var aiPlay = false;
var aiHasWinPos = false;

// Modal
var lastAi;
var lastMenu = "none";
var currMenu = "none";
var screenVisible = true;
var isAiLevelOpen = false;
var isAdvertShow = false;

// Choose background
var bg = {
    current : 0,
    src : ["asset/img/bg-0.jpg", "asset/img/bg-1.jpg", "asset/img/bg-2.jpg"]
}

// Sound
var uiSound;
